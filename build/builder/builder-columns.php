<?php 
    $mainheading = get_sub_field('heading');
?>

<section class="builder columns">
    <h2 class="centred"><?php echo $mainheading; ?></h2>
    <div class="grid-wrapper">
        <?php
        if( have_rows('columns') ):

        // loop through the rows of data
        while ( have_rows('columns') ) : the_row(); 
            $heading = get_sub_field('column_heading');
            $content = get_sub_field('column_content');
        ?>
            <article class="grid-item">
                <div class="content-wrapper">
                    <?php if ($heading) : ?> <h3><?php echo $heading; ?></h3><?php endif; ?>
                    <?php if ($content) : ?><div class="content">
                        <?php echo $content; ?>
                    </div><?php endif; ?>
                </div>
            </article>
        <?php
        endwhile;

        else :

            // no rows found

        endif;
        ?>
    </div>
</section>








