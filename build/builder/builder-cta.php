<?php 
$form = '';
$heading = get_sub_field('heading');
$content = get_sub_field('content');
$button = get_sub_field('purchase_button');
$note = get_sub_field('note');
$splitup = explode(" ", $heading);
?>


<?php 
if (get_field('status')) : 
    if (get_field('groups')):
        $groups = '<input type="hidden" name="group_id" value="' . get_field("groups") . '">';
    else : 
        $groups = '';
    endif;

    if (get_field('optin_success_message')) :
        $optin_success = get_field('optin_success_message');
    else : 
        $optin_success = get_field('default_optin_success_message', 'option'); 
    endif; 

    $form = '
        <form method="post" data-womc>
            <div class="womc-fields" data-womc-fields>
                <input type="email" name="email" placeholder="email address" aria-required="true" aria-invalid="false">
                <input type="submit" value="Sign Up" name="subscribe">
                <input type="hidden" name="list_id" value="' . get_field("list") . '">
                <input type="hidden" name="signup" value="' . get_the_title() . '">' . $groups . '

                <input type="hidden" name="action" value="womc_subscribe">' .
                wp_nonce_field( "womc_subscribe_nonce", "post_mc_sub" ) . '
            </div>
            <div class="womc-success-message hide" data-womc-success>
                ' . $optin_success . '
            </div>
            <div class="womc-already-sub-message hide" data-womc-error>
                <p>It looks like you\'re already subscribed. Yay!</p>
            </div>
        </form>';

endif; ?>
<?php 
    $note_with_form = str_replace("%%subscribe_form%%", $form, $note);
?>


<section class="builder cta" id="<?php echo $splitup[0]; ?>">
    <h2><?php echo $heading; ?></h2>
    <div class="content-wrapper">
        <div class="content">
            <?php echo $content; ?>
        </div>
        <div class="purchase-form-wrapper">
            <a href="#purchase-form" class="open-popup-link"><?php echo $button['button_text']; ?></a>
            <div id="purchase-form" class="mfp-hide overlay-popup">
                <?php
                    gravity_form( $button['gravity_form']['id'], true, false, false, null, true, '1', true );

                ?>
            </div>
            <script>
                jQuery(document).ready(function() {
                    jQuery('.open-popup-link').magnificPopup({
                        type:'inline',
                        showCloseBtn: false,
                        midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
                    });
                });
            </script>
        </div>
        <div class="note">
            <?php echo $note_with_form; ?>
        </div>
    </div>
</section>