<?php
    $arrow = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="15" height="20" viewBox="0 0 12 5">
<path d="M8.398 5.75q0 0.102-0.078 0.18l-3.641 3.641q-0.078 0.078-0.18 0.078t-0.18-0.078l-3.641-3.641q-0.078-0.078-0.078-0.18t0.078-0.18l0.391-0.391q0.078-0.078 0.18-0.078t0.18 0.078l3.070 3.070 3.070-3.070q0.078-0.078 0.18-0.078t0.18 0.078l0.391 0.391q0.078 0.078 0.078 0.18z"></path>
</svg>';
?>
   

<section class="builder exercise-section">
    <header>
        <h6 class="exercise-meta">Exercise</h6>
        <h3 class="exercise-title"><?php echo get_sub_field('heading'); ?></h3>
    </header>
    <dt class="exercise"><?php echo get_sub_field('exercise'); ?></dt>
    <?php if ($answer = get_sub_field('answer')) : ?>
        <h4 class="solution-heading"><?php echo __('Solution') . ' ' . $arrow; ?></h4>
        <dd class="solution">
            <?php echo $answer; ?>
        </dd>
    <?php endif; ?>
</section>