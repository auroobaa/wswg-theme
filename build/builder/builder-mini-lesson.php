<?php 
    $type_id = get_sub_field('mini_lesson_type');
    $type = get_term($type_id);
    $editor = get_sub_field('text');
    $heading = get_sub_field('heading');
?>


<article class="builder mini-lesson <?php echo $type->slug; ?>" id="mini-lesson-<?php echo sanitize_title_with_dashes($heading); ?>">
    <header>
        <h6 class="mini-lesson__type"><?php echo $type->name; ?></h6>
        <h3><?php echo $heading; ?></h3>
    </header>
    <?php echo $editor; ?>
</article>