<?php 
    $editor = get_sub_field('text');
    if (!$editor) {
        $editor = get_sub_field('editor');
    }

?>


<div class="builder text">
    <?php echo $editor; ?>
</div>