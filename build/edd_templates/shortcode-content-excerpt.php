<?php 
$item_prop = edd_add_schema_microdata() ? ' itemprop="description"' : ''; 
$course = get_field('courses', get_the_ID());
if ( get_the_excerpt($course) ) :
    echo '<p' . $item_prop . '>' . get_the_excerpt($course) . '</p></div>';
endif; ?>
