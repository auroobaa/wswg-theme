<?php 
$item_prop = edd_add_schema_microdata() ? ' itemprop="name"' : ''; 
$course = get_field('courses', get_the_ID());

echo '<div class="course-info"><h3' . $item_prop . '>' . get_the_title() . '</h3>';
?>
