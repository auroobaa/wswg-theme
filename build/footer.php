<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WSWG_Theme
 */

$mailchimp = get_field('settings', 'option');
$list = $mailchimp['list'];
$groups = $mailchimp['groups'];
if (!is_front_page()) {
    $unfixed_current_title = get_the_title();
    $current_title = wp_strip_all_tags($unfixed_current_title, true);
} elseif (is_front_page()) {
    $current_title = 'Home';
} elseif (is_home()) {
    $current_title = 'Blog Index';
}
$privacy_link = get_field('privacy_policy', 'option');

?>

	</div><!-- #content -->
	<footer id="colophon" class="site-footer" role="contentinfo">
       <?php if (!get_field('status')) : ?>
        <div class="optin-footer onsite">
            <div class="optin-blurb">
                <?php the_field('default_optin_message', 'option'); ?>
            </div>
            <form method="post" action="<?php echo admin_url('admin-ajax.php'); ?>" data-womc>
                <div class="womc-fields" data-womc-fields>
                    <input type="email" name="email" placeholder="email address" aria-required="true" aria-invalid="false">
                    <input type="submit" value="Sign Up" name="subscribe">
                    <input type="hidden" name="list_id" value="<?php echo $list; ?>">
                    <input type="hidden" name="signup" value="<?php echo $current_title; ?>">
                    <?php 
                    if ($groups): ?>
                        <input type="hidden" name="group_id" value="<?php echo $groups; ?>">
                    <?php endif; ?>
                    <input type="hidden" name="action" value="womc_subscribe">
                    <?php wp_nonce_field( 'womc_subscribe_nonce', 'post_mc_sub' ); ?>
                </div>
                <div class="womc-success-message hide" data-womc-success>
                   <?php
                        the_field('default_optin_success_message', 'option'); 
                    ?>
                </div>
                <div class="womc-already-sub-message hide" data-womc-error>
                    <p>It looks like you're already subscribed. Yay!</p>
                </div>
            </form>

        </div>
        <?php else : ?>
            <div class="optin-footer onsite">
                <div class="optin-blurb onsite">
                    <?php if (get_field('opt_in_blurb')) : ?>
                        <?php the_field('opt_in_blurb'); ?>
                    <?php else : ?>
                        <?php the_field('default_optin_message', 'option'); ?>
                    <?php endif; ?>
                </div>   
                <form method="post" action="<?php echo admin_url('admin-ajax.php'); ?>" data-womc>
                    <div class="womc-fields" data-womc-fields>
                        <input type="email" name="email" placeholder="email address" aria-required="true" aria-invalid="false">
                        <input type="submit" value="Sign Up" name="subscribe">
                        <input type="hidden" name="list_id" value="<?php the_field('list');?>">
                        <input type="hidden" name="signup" value="<?php the_title();?>">
                        <?php 
                        if (get_field('groups')): ?>
                            <input type="hidden" name="group_id" value="<?php the_field('groups');?>">
                        <?php endif; ?>
                        <input type="hidden" name="action" value="womc_subscribe">
                        <?php wp_nonce_field( 'womc_subscribe_nonce', 'post_mc_sub' ); ?>
                    </div>
                    <div class="womc-success-message hide" data-womc-success>
                       <?php if (get_field('optin_success_message')) :
                            the_field('optin_success_message');
                        else : 
                            the_field('default_optin_success_message', 'option'); 
                        endif; ?>
                    </div>
                    <div class="womc-already-sub-message hide" data-womc-error>
                        <p>It looks like you're already subscribed. Yay!</p>
                    </div>
                </form>
            </div>
        <?php endif; ?>
<!--      <section class="bottom-hero">-->
<!--
        <a class="bottom-hero-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
            <img src="<?php //echo get_template_directory_uri()?>/imgs/full_logo_colour_final.svg">
        </a>
-->
        <?php $description = get_bloginfo( 'description', 'display' );
			//if ( $description || is_customize_preview() ) : ?>
<!--
				<div id="site-desc-bottom">
                   <p class="site-description-bottom"><?php //echo $description; /* WPCS: xss ok. */ ?></p>
                </div>
-->
			<?php
			//endif; ?>
<!--      </section>-->
		<div class="site-info">
			<span class="site-credit"><?php printf( 'A %2$s project.', 'wanderoak', '<a href="http://wanderoak.co/" rel="designer">wanderoak</a>' ); ?></span>
		    <span class="site-copyright">Copyright <?php echo date("Y"); ?> <?php echo get_bloginfo('name'); ?>. All rights reserved.<span class="privacy-link"><a href="<?php echo $privacy_link; ?>">Privacy</a></span></span>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script>
  (function(d) {
    var config = {
      kitId: 'zya2kfu',
      scriptTimeout: 3000,
      async: true
    },
    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
  })(document);
</script>
<script>
// Select all links with hashes
jQuery('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = jQuery(this.hash);
      target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        jQuery('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = jQuery(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
</script>

</body>
</html>
