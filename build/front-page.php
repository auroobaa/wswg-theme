<?php
/**
 * The Front Page Template 
 *
 * @package WSWG_Theme
 */

get_header( 'single' ); 
$articles = get_field('latest_articles');
$ad = get_field('ad');
$hcategories = get_field('categories');
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main home-main <?php if (!$ad['display']) : echo 'no-ad'; endif; ?>" role="main">

		<?php

			if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>

			<?php
			endif;
            if ($articles['articles_display'] != 10) {
                $args = array( 'post_status' => 'publish', 'numberposts' => $articles['articles_display'] );
            } else {
                $args = array( 'post_status' => 'publish' );
            }
            $recent_posts = wp_get_recent_posts( $args );
            ?>
            <section class="recent-articles">
                <header><h2><?php echo $articles['heading']; ?></h2></header>
                <?php
                foreach( $recent_posts as $recent ){
                    echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';
                }
                wp_reset_query();
                ?>
                <div class="all-articles-link-wrapper">
                    <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>"><?php echo $articles['all_articles_label']; ?></a>
                </div>
            </section>
            <?php if ($ad['display']) : ?>
            <section class="ad">
                <header>
                    <h2 class="heading"><?php echo $ad['heading']; ?></h2>
                    <p class="sub-heading"><?php echo $ad['subheading']; ?></p>
                </header>
                <div class="content">
                    <?php echo $ad['content']; ?>
                    <div class="button-wrapper">
                        <a href="<?php echo $ad['button']['url']; ?>"><?php echo $ad['button']['text']; ?></a>
                    </div>
                </div>
            </section>
            <?php endif; ?>
            <section class="home-categories">
                <header>
                    <h2><?php echo $hcategories['heading']; ?></h2>
                </header>
                <?php
                $categories = get_categories( array(
                    'orderby' => 'name',
                    'parent'  => 0
                ) );

                foreach ( $categories as $category ) {
                    printf( '<a href="%1$s">%2$s</a>',
                        esc_url( get_category_link( $category->term_id ) ),
                        esc_html( $category->name )
                    );
                }
                ?>
            </section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
