<?php

function wswg_courses_cpt() {
  $labels = array(
    'name'               => _x( 'We Start With Good Courses', 'post type general name' ),
    'singular_name'      => _x( 'Course', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'guest-party' ),
    'add_new_item'       => __( 'Add New Course' ),
    'edit_item'          => __( 'Edit Course' ),
    'new_item'           => __( 'New Course' ),
    'all_items'          => __( 'All Courses' ),
    'view_item'          => __( 'View Course' ),
    'search_items'       => __( 'Search Courses' ),
    'not_found'          => __( 'No courses found' ),
    'not_found_in_trash' => __( 'No courses found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Courses',
    
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'We Start With Good Courses',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'excerpt', 'thumbnail' ),
    'has_archive'   => false,
    'publicly_queryable' => true,
    'menu_icon'     => 'dashicons-welcome-learn-more',
    'hierarchical'  => true,
    'rewrite'       => array( 'slug' => 'courses', 'with_front' => false ),
//    'capability_type' => 'wswgcourses',
//    'capabilities' => array(
//                    'publish_posts' => 'publish_wswgcourses',
//                    'edit_posts' => 'edit_wswgcourses',
//                    'edit_others_posts' => 'edit_others_wswgcourses',
//                    'delete_posts' => 'delete_wswgcourses',
//                    'delete_others_posts' => 'delete_others_wswgcourses',
//                    'read_private_posts' => 'read_private_wswgcourses',
//                    'edit_post' => 'edit_wswgcourses',
//                    'delete_post' => 'delete_wswgcourses',
//                    'read_post' => 'read_wswgcourses',
//    ),
  );
  register_post_type( 'wswg-courses', $args ); 
}
add_action( 'init', 'wswg_courses_cpt' );

function wswg_courses_category() {
  $labels = array(
    'name'              => _x( 'Course Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Course Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Course Categories' ),
    'all_items'         => __( 'All Course Categories' ),
    'parent_item'       => __( 'Parent Course Category' ),
    'parent_item_colon' => __( 'Parent Course Category:' ),
    'edit_item'         => __( 'Edit Course Category' ), 
    'update_item'       => __( 'Update Course Category' ),
    'add_new_item'      => __( 'Add New Course Category' ),
    'new_item_name'     => __( 'New Course Category' ),
    'menu_name'         => __( 'Course Categories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'wswg_course_categories', 'wswg-courses', $args );
}
add_action( 'init', 'wswg_courses_category', 0 );

/**
 * Creating a hierarchial taxonomy for modules
 */
function wswg_lesson_modules() {
  $labels = array(
    'name'              => _x( 'Modules', 'taxonomy general name' ),
    'singular_name'     => _x( 'Module', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Modules' ),
    'all_items'         => __( 'All Modules' ),
    'parent_item'       => __( 'Parent Module' ),
    'parent_item_colon' => __( 'Parent Module:' ),
    'edit_item'         => __( 'Edit Module' ), 
    'update_item'       => __( 'Update Module' ),
    'add_new_item'      => __( 'Add Module' ),
    'new_item_name'     => __( 'New Module' ),
    'menu_name'         => __( 'Modules' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'wswg_lesson_modules', 'wswg-courses', $args );
}
//add_action( 'init', 'wswg_lesson_modules', 0 );
