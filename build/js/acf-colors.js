jQuery(document).ready(function() {
    acf.add_filter('color_picker_args', function( args, field ){

        // do something to args
        args.palettes = ['#A6B477', '#299076', '#37C09D', '#FFD17B', '#F76D4E', '#2D2E32', '#F7F7F7']

        // return
        return args;

    });
});