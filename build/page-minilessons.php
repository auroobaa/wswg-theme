<?php
/**
 * Template Name: Mini PHP Lessons
 *
 * @package wswg
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
            <header class="page-builder-header <?php if (get_field('centre_page_title')): echo 'centred more-margin'; endif; ?>">
                <?php
                    the_title( '<h1 class="entry-title">', '</h1>' );
                ?>
            </header><!-- .entry-header -->
            <section class="mini-php-lessons">
            
            
            
            </section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer( 'bare' );


