<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package first-theme
 */

if ( ! is_active_sidebar( 'wswg-articles' ) ) {
	return;
}
?>

<aside id="wswg-author" class="widget-area" role="complementary">
	<?php dynamic_sidebar( 'wswg-articles' ); ?>
</aside><!-- #secondary -->
