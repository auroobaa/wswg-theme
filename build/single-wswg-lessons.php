<?php
/**
 * The template for displaying all single course pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WSWG_Theme
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
            $course_id = wp_get_post_parent_id(get_the_ID());
            if (!$course_id) {
                echo '<p>Whoops! Be sure to attach this lesson to a course first!<p>';
            }

            if ( !edd_cr_user_has_access(get_current_user_id(), $course_id) ) {

                $restricted_to = get_post_meta( $course_id, '_edd_cr_restricted_to', true );
                $edd = edd_cr_user_can_access('', $restricted_to);
                echo $edd['message'];

            } else {
                while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/content', 'lesson' );

                    //the_post_navigation();

                    // If comments are open or we have at least one comment, load up the comment template.
//                    if ( comments_open() || get_comments_number() ) :
//                        comments_template();
//                    endif;

                endwhile; // End of the loop.
            }
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
    <script>
    jQuery( ".solution-heading" ).click(function() {
      jQuery(this).next().slideToggle();
    });
    </script>
<?php
get_sidebar('courses');
get_footer( 'bare' );
