<?php
/**
 * Template part for displaying course content.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WSWG_Theme
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
        <div class="entry-header-content">
            <?php
                if ( is_single() ) {
                    the_title( '<h1 class="entry-title">', '</h1>' );
                } else {
                    the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
                } ?>
        </div>

	</header><!-- .entry-header -->
    <?php //wswg_list_lessons(); ?>
	<div class="entry-content">
		<?php
			the_field('course_introduction');
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->