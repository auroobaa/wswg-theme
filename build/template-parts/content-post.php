<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WSWG_Theme
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
        <div class="entry-header-content">

            <?php
                if ( is_single() ) {
                    the_title( '<h1 class="entry-title">', '</h1>' );
                } else {
                    the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
                } ?>
        </div>

        <?php if ( 'post' === get_post_type() ) : ?>
        <div class="entry-meta">
            <?php wswg_posted_on(); ?>
        </div><!-- .entry-meta -->
        <?php
        endif; ?>
        <?php if (get_field('disclaimernote')) : ?>
	    <div class="disclaimernote">
	        <?php the_field('disclaimernote'); ?>
	    </div>
	    <?php endif; ?>

	</header><!-- .entry-header -->

	<div class="entry-content">
	    <div class="article-content">
            <?php
                the_content( sprintf(
                    /* translators: %s: Name of current post. */
                    wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'wswg' ), array( 'span' => array( 'class' => array() ) ) ),
                    the_title( '<span class="screen-reader-text">"', '"</span>', false )
                ) );

                wp_link_pages( array(
                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wswg' ),
                    'after'  => '</div>',
                ) );
            ?>
            </div>
        <?php
            get_sidebar('articles');
		?>
		
	</div><!-- .entry-content -->
	<?php if (get_field('status')) : ?>
	<footer class="optin-footer onarticle">
        <div class="optin-blurb">
            <?php if (get_field('opt_in_blurb')) : ?>
                <?php the_field('opt_in_blurb'); ?>
            <?php else : ?>
                <?php the_field('default_optin_message', 'option'); ?>
            <?php endif; ?>
        </div>   
        <form method="post" action="<?php echo admin_url('admin-ajax.php'); ?>" data-womc>
            <div class="womc-fields" data-womc-fields>
                <input type="email" name="email" placeholder="email address" aria-required="true" aria-invalid="false">
                <input type="submit" value="Sign Up" name="subscribe">
                <input type="hidden" name="list_id" value="<?php the_field('list');?>">
                <input type="hidden" name="signup" value="<?php the_title();?>">
                <?php 
                if (get_field('groups')): ?>
                    <input type="hidden" name="group_id" value="<?php the_field('groups');?>">
                <?php endif; ?>
                <input type="hidden" name="action" value="womc_subscribe">
                <?php wp_nonce_field( 'womc_subscribe_nonce', 'post_mc_sub' ); ?>
            </div>
            <div class="womc-success-message hide" data-womc-success>
               <?php if (get_field('optin_success_message')) :
                    the_field('optin_success_message');
                else : 
                    the_field('default_optin_success_message', 'option'); 
                endif; ?>
            </div>
            <div class="womc-already-sub-message hide" data-womc-error>
                <p>It looks like you're already subscribed. Yay!</p>
            </div>
        </form>
	    
	</footer>
	<?php endif; ?>
</article><!-- #post-## -->
