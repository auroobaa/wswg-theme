<nav id="logged-in-site-navigation" class="logged-in-navigation" role="navigation">
    <?php 
        $current_post_type = get_post_type( get_the_ID() );
        if ( $current_post_type == 'wswg-courses' ) {
            $page_title = get_the_title();
        } elseif ( $current_post_type == 'wswg-lessons' ) {
            $course_id =  wp_get_post_parent_id( get_the_ID() );
            $page_title = get_the_title( $course_id );
            $page_link = get_permalink( $course_id );
        } elseif (is_front_page() || $current_post_type == 'post' ) {
            $page_title = '';
        } else {
            $page_title = get_the_title();
        }
        $page_title_tag = '';
        if ($page_link) : 
            $page_title_tag = 'Course &rarr; <a href="' . $page_link . '">' . $page_title . '</a>';
        else :
            $page_title_tag = $page_title;
        endif;
    ?>
    <div class="page-header-title"><?php echo $page_title_tag; ?></div>
    <div class="right-side"><?php cleanernav('logged-in'); ?></div>
</nav>