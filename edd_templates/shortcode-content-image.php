<?php 
if (  $course = get_field('courses', get_the_ID()) ) :
    echo '<div class="featured-image">' . get_the_post_thumbnail($course) . '</div>';
endif; ?>


