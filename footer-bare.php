<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WSWG_Theme
 */

if (!is_front_page()) {
    $unfixed_current_title = get_the_title();
    $current_title = wp_strip_all_tags($unfixed_current_title, true);
} elseif (is_front_page()) {
    $current_title = 'Home';
} elseif (is_front_page() && !is_home()) {
    $current_title = 'Blog Index';
}
$privacy_link = get_field('privacy_policy', 'option');

?>

	</div><!-- #content -->
	<footer id="colophon" class="site-footer" role="contentinfo">

		<div class="site-info">
			<span class="site-credit"><?php printf( 'A %2$s project.', 'wanderoak', '<a href="http://wanderoak.co/" rel="designer">wanderoak</a>' ); ?></span>
		    <span class="site-copyright">Copyright <?php echo date("Y"); ?> <?php echo get_bloginfo('name'); ?>. All rights reserved.<span class="privacy-link"><a href="<?php echo $privacy_link; ?>">Privacy</a></span></span>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script>
  (function(d) {
    var config = {
      kitId: 'zya2kfu',
      scriptTimeout: 3000,
      async: true
    },
    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
  })(document);
</script>
<script>
// Select all links with hashes
jQuery('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = jQuery(this.hash);
      target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        jQuery('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = jQuery(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
</script>
</body>
</html>
