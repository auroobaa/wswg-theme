<?php
/**
 * WordPress backend modifications by Wanderoak
 *
 * This is functionality added by Aurooba Ahmed
 *
 * @package WSWG_Theme
 */


/**
 * Add Visual Blocks external plugin to TinyMCE
 */

function add_mceplugins() {
     $plugin = array('visualblocks');
     $plugins_array = array();

     foreach ($plugin as $plugin ) {
          $plugins_array[ $plugin ] = get_stylesheet_directory_uri() . '/js/'. $plugin .'/plugin.js';
     }
     return $plugins_array;
}
add_filter('mce_external_plugins', 'add_mceplugins');

function register_visualblocks($in) {
    $in['visualblocks_default_state'] = 'true';
    return $in;
}
add_filter('tiny_mce_before_init', 'register_visualblocks'); 

/**
 * Add Wanderoak credit to footer in backend
 */

function remove_footer_admin () {

echo 'Fueled by <a href="http://www.wordpress.org" target="_blank">WordPress</a> | Designed & Developed by <a href="http://wanderoak.co" target="_blank">Wanderoak</a>';

}

add_filter('admin_footer_text', 'remove_footer_admin');

/**
* Add Support Widget
**/ 

function support_add_dashboard_widgets() {

  wp_add_dashboard_widget('wp_dashboard_widget', 'Support', 'support_info');
    
// Globalize the metaboxes array, this holds all the widgets for wp-admin
 
 	global $wp_meta_boxes;
 	
 	// Get the regular dashboard widgets array 
 	// (which has our new widget already but at the end)
 
 	$normal_dashboard = $wp_meta_boxes['dashboard']['normal']['core'];
 	
 	// Backup and delete our new dashboard widget from the end of the array
 
 	$example_widget_backup = array( 'wp_dashboard_widget' => $normal_dashboard['wp_dashboard_widget'] );
 	unset( $normal_dashboard['wp_dashboard_widget'] );
 
 	// Merge the two arrays together so our widget is at the beginning
 
 	$sorted_dashboard = array_merge( $example_widget_backup, $normal_dashboard );
 
 	// Save the sorted array back into the original metaboxes 
 
 	$wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;

}

add_action('wp_dashboard_setup', 'support_add_dashboard_widgets' );

function support_info() { ?>

<div class="custom-welcome-panel-content">
	<h3 style="padding-left: 0; font-size: 16px;">Welcome to Your Dashboard!</h3>
	<p><?php _e( 'This dashboard is powered by Wordpress and customized to fit your needs by Wanderoak' ); ?></p>
	<div class="welcome-panel-column-container" style="overflow: hidden;">
	<div class="welcome-panel-column" style="float: left; width: 40%; margin-right: 2em;">
		<h4><?php _e( 'Next Steps' ); ?></h4>
		<ul>
		<?php if ( 'page' == get_option( 'show_on_front' ) && ! get_option( 'page_for_posts' ) ) : ?>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-edit-page">' . __( 'Edit your front page' ) . '</a>', get_edit_post_link( get_option( 'page_on_front' ) ) ); ?></li>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-add-page">' . __( 'Add additional pages' ) . '</a>', admin_url( 'post-new.php?post_type=page' ) ); ?></li>
		<?php elseif ( 'page' == get_option( 'show_on_front' ) ) : ?>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-write-blog">' . __( 'Create an article' ) . '</a>', admin_url( 'post-new.php' ) ); ?></li>
		<?php else : ?>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-write-blog">' . __( 'Write your first article' ) . '</a>', admin_url( 'post-new.php' ) ); ?></li>
		<?php endif; ?>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-view-site">' . __( 'View your site' ) . '</a>', home_url( '/' ) ); ?></li>
		</ul>
			<div>
		<h4><?php _e( "Questions?" ); ?></h4>
		<a class="button button-primary button-hero load-customize hide-if-no-customize" href="mailto:hey@wanderoak.co"><?php _e( 'Email Us!' ); ?></a>
<!--			<p class="hide-if-no-customize"><?php printf( __( 'or, <a href="%s">edit your site settings</a>' ), admin_url( 'options-general.php' ) ); ?></p>-->
	</div>
	</div>
	<div class="welcome-panel-column welcome-panel-last">
		<h4><?php _e( 'More Actions' ); ?></h4>
		<ul>
			<li><?php printf( '<div class="welcome-icon welcome-widgets-menus">' . __( 'Manage your <a href="%1$s">navigation menu</a>' ) . '</div>', admin_url( 'nav-menus.php' ) ); ?></li>
			<li><?php printf( '<a href="%s" class="welcome-icon welcome-comments">' . __( 'Turn comments on or off' ) . '</a>', admin_url( 'options-discussion.php' ) ); ?></li>
			<!--<li><php printf( '<a href="%s" class="welcome-icon welcome-learn-more">' . __( 'Learn more about getting started' ) . '</a>', __( 'http://localhost/polishedarrow/wp-admin/index.php?page=wo-walkthrough' ) ); ?></li>-->
		</ul>
	</div>
       <br />
        <h4><?php _e( "Confused?" ); ?></h4>
            <a class="button button-primary button-hero load-customize hide-if-no-customize" href="https://cseb-scbe.org/wpmarine/wp-admin/index.php?page=wo-walkthrough"><?php _e( 'Watch the tutorial!' ); ?></a>
    <!--			<p class="hide-if-no-customize"><?php printf( __( 'or, <a href="%s">edit your site settings</a>' ), admin_url( 'options-general.php' ) ); ?></p>-->
	</div>
</div>
 
  
<?php }


class PostAdminWordCount {

    function init() {
        if (is_admin()) {
            add_filter('manage_edit-post_sortable_columns', array(&$this, 'pwc_column_register_sortable'));
            add_filter('posts_orderby', array(&$this, 'pwc_column_orderby'), 10, 2);
            add_filter("manage_posts_columns", array(&$this, "pwc_columns"));
            add_action("manage_posts_custom_column", array(&$this, "pwc_column"));
            add_action("admin_footer-edit.php",array(&$this, "pwc_update_date"));
            add_action("admin_head-edit.php",array(&$this, "pwc_get_date"));
            
        }
    }

    //=============================================
    // Add new columns to action post type
    //=============================================
    function pwc_columns($columns) {
        $columns["post_word_count"] = "Word Count";
        return $columns;
    }

    //=============================================
    // Add data to new columns of action post type
    //=============================================
    function pwc_column($column) {
        global $post, $pwc_last;
        if ("post_word_count" == $column) {
            // Grab a fresh word count
            $word_count = str_word_count($post->post_content);
            echo $word_count;
        }
    }

    //=============================================
    // Queries to run when sorting
    // new columns of action post type
    //=============================================
    function pwc_column_orderby($orderby, $wp_query) {
        global $wpdb;

        if ('post_word_count' == @$wp_query->query['orderby'])
            $orderby = "(SELECT CAST(meta_value as decimal) FROM $wpdb->postmeta WHERE post_id = $wpdb->posts.ID AND meta_key = '_post_word_count') " . $wp_query->get('order');

        return $orderby;
    }

    //=============================================
    // Make new columns to action post type sortable
    //=============================================
    function pwc_column_register_sortable($columns) {
        $columns['post_word_count'] = 'post_word_count';
        return $columns;
    }
    
    function pwc_get_date(){
        global $post, $pwc_last;
        // Check last updated
        $pwc_last = get_option('pwc_last_checked');

        // Check to make sure we have a post and post type
		if ( $post && $post->post_type ){
			
			// Grab all posts with post type
			$args = array(
				'post_type' => $post->post_type,
				'posts_per_page' => -1
				);

			// Grab the posts
			$post_list = new WP_Query($args);
			if ( $post_list->have_posts() ) : while ( $post_list->have_posts() ) : $post_list->the_post(); 
				
				// Grab a fresh word count
	            $word_count = str_word_count($post->post_content);

	            // If post has been updated since last check
	            if ($post->post_modified > $pwc_last || $pwc_last == "") {
	            	// Grab word count from post meta
	                $saved_word_count = get_post_meta($post->ID, '_post_word_count', true);
	                // Check if new wordcount is different than old word count
	                if ($saved_word_count != $word_count || $saved_word_count == "") {
	                	// Update word count in post meta
	                    update_post_meta($post->ID, '_post_word_count', $word_count, $saved_word_count);
	                }
	            }
			endwhile; 
			endif;

			// Let WordPress do it's thing
			wp_reset_query();
		}
    }
    
    function pwc_update_date(){
    	// Save the last time this page was generated
        $current_date = current_time('mysql');
        update_option('pwc_last_checked', $current_date);
    }

}

$postAdminWordCount = new PostAdminWordCount();
$postAdminWordCount->init();

function my_custom_login_logo() {
    echo '<style type="text/css">
        @import url("//hello.myfonts.net/count/32eb97");
        @import url("https://use.typekit.net/zya2kfu.css");


        /* @import must be at top of file, otherwise CSS will not work */



        @font-face {
            font-family: "tisa";
            src: url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_0_0.eot");
            src: url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_0_0.eot?#iefix") format("embedded-opentype"),
                url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_0_0.woff2") format("woff2"),
                url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_0_0.woff") format("woff"),
                url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_0_0.ttf") format("truetype");
            font-weight: normal;
            font-style: italic;

        }


        @font-face {
            font-family: "tisa";
            src: url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_1_0.eot");
            src: url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_1_0.eot?#iefix") format("embedded-opentype"),
                url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_1_0.woff2") format("woff2"),
                url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_1_0.woff") format("woff"),
                url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_1_0.ttf") format("truetype");
            font-weight: 700;
            font-style: italic;
        }


        @font-face {
            font-family: "tisa";
            src: url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_2_0.eot");
            src: url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_2_0.eot?#iefix") format("embedded-opentype"),
                url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_2_0.woff2") format("woff2"),
                url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_2_0.woff") format("woff"),
                url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_2_0.ttf") format("truetype");
            font-weight: 700;
            font-style: normal;
        }


        @font-face {
            font-family: "tisa";
            src: url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_3_0.eot");
            src: url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_3_0.eot?#iefix") format("embedded-opentype"),
                url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_3_0.woff2") format("woff2"),
                url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_3_0.woff") format("woff"),
                url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_3_0.ttf") format("truetype");
            font-weight: 200;
            font-style: italic;
        }


        @font-face {
            font-family: "tisa";
            src: url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_4_0.eot");
            src: url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_4_0.eot?#iefix") format("embedded-opentype"),
                url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_4_0.woff2") format("woff2"),
                url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_4_0.woff") format("woff"),
                url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_4_0.ttf") format("truetype");
            font-weight: normal;
            font-style: normal
        }


        @font-face {
            font-family: "tisa";
            src: url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_5_0.eot");
            src: url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_5_0.eot?#iefix") format("embedded-opentype"),
                url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_5_0.woff2") format("woff2"),
                url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_5_0.woff") format("woff"),
                url("https://wanderoak.co/wp-content/uploads/fontkit/32EB97_5_0.ttf") format("truetype");
            font-weight: 200;
            font-style: normal;
        }
        
        
        
        h1 a { 
            background-image:url(https://westartwithgood.co/wp-content/uploads/2018/02/180111_WSWG_Logo_Black.png) !important; 
            background-size: 325px !important;
            width: auto !important;
            height: 42px !important;
        }
        body {
            background-color: #f7f7f7 !important;
            color: #5a5752 !important;
            font-size: 18px;
            font-family: tisa, serif;
        }
        
        .login label {
            color: #2d2e32 !important;
            font-size: 18px;
            font-family: tisa, serif;
        }
        .login form {
            box-shadow: none !important;
            -webkit-box-shadow: none !important;
        }
        .login form .input, .login input[type=text] {
            padding: 8px 4px 4px 4px;
        }
        .login #login_error {
        font-size: 14px;
        }
        #loginform #wp-submit {
            -webkit-appearance: none;
            border: 1px solid #299076;
            border-bottom: 1px solid #299076;
            background: #299076;
            box-shadow: none !important;
            color: white;
            text-shadow: 0 0 0 !important;
            font-family: "jaf-facitweb", sans-serif;
            font-weight: bold;
            font-style: italic;
            font-size: 20px;
            letter-spacing: 0.02em;
            height: auto;
            padding: 0.2em 0.5em;
        }
        input:-webkit-autofill:focus {
            -webkit-box-shadow:0 0 0 50px white inset !important;
            }
            input:-webkit-autofill {
            -webkit-box-shadow:0 0 0 50px white inset !important;
            }
    </style>';
}

add_action('login_head', 'my_custom_login_logo');

// Remove ACF inline styles for WYSIWYG
function my_acf_input_admin_footer() { ?>
	<script type="text/javascript">
		(function($) {
			acf.add_action('wysiwyg_tinymce_init', function( ed, id, mceInit, $field ){
				$(".acf-field .acf-editor-wrap iframe").removeAttr("style");
			});
		})(jQuery);	
	</script>
<?php }
add_action('acf/input/admin_footer', 'my_acf_input_admin_footer');

function my_acf_admin_head() {
	?>
	<style type="text/css">

        .acf-editor-wrap iframe {
            width: 100%;
            display: block;
            height: 300px;
        }
        
        .small-tinymce .acf-editor-wrap iframe {
            height: 100px;
            width: 100%;
            display: block;
        }

	</style>

	<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');

add_action( 'contextual_help', 'wptuts_screen_help', 10, 3 );
function wptuts_screen_help( $contextual_help, $screen_id, $screen ) {
 
    // The add_help_tab function for screen was introduced in WordPress 3.3.
    if ( ! method_exists( $screen, 'add_help_tab' ) )
        return $contextual_help;
 
    global $hook_suffix;
 
    // List screen properties
    $variables = '<ul style="width:50%;float:left;"> <strong>Screen variables </strong>'
        . sprintf( '<li> Screen id : %s</li>', $screen_id )
        . sprintf( '<li> Screen base : %s</li>', $screen->base )
        . sprintf( '<li>Parent base : %s</li>', $screen->parent_base )
        . sprintf( '<li> Parent file : %s</li>', $screen->parent_file )
        . sprintf( '<li> Hook suffix : %s</li>', $hook_suffix )
        . '</ul>';
 
    // Append global $hook_suffix to the hook stems
    $hooks = array(
        "load-$hook_suffix",
        "admin_print_styles-$hook_suffix",
        "admin_print_scripts-$hook_suffix",
        "admin_head-$hook_suffix",
        "admin_footer-$hook_suffix"
    );
 
    // If add_meta_boxes or add_meta_boxes_{screen_id} is used, list these too
    if ( did_action( 'add_meta_boxes_' . $screen_id ) )
        $hooks[] = 'add_meta_boxes_' . $screen_id;
 
    if ( did_action( 'add_meta_boxes' ) )
        $hooks[] = 'add_meta_boxes';
 
    // Get List HTML for the hooks
    $hooks = '<ul style="width:50%;float:left;"> <strong>Hooks </strong> <li>' . implode( '</li><li>', $hooks ) . '</li></ul>';
 
    // Combine $variables list with $hooks list.
    $help_content = $variables . $hooks;
 
    // Add help panel
    $screen->add_help_tab( array(
        'id'      => 'wptuts-screen-help',
        'title'   => 'Screen Information',
        'content' => $help_content,
    ));
 
    return $contextual_help;
}

function yoasttobottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

include_once('acf-smart-button/acf-smart-button.php');
include_once('acf-gravity-forms/acf-gravity_forms.php');

/**
* remove the register link from the wp-login.php script
*/
add_filter('option_users_can_register', function($value) {
    $script = basename(parse_url($_SERVER['SCRIPT_NAME'], PHP_URL_PATH));
 
    if ($script == 'wp-login.php') {
        $value = false;
    }
 
    return $value;
});

add_action( 'acf/input/admin_enqueue_scripts', function() {
  wp_enqueue_script( 'acf-custom-colors', get_template_directory_uri() . '/js/acf-colors.js', 'acf-input', '1.0', true );
});


function block_title( $title, $field, $layout, $i ) {
	
	// remove layout title from text
	// load text sub field
	if( $text = get_sub_field('heading') ) {
		
		$title .= ': ' . $text;
		
	}
	
	// return
	return $title;
	
}

// name
add_filter('acf/fields/flexible_content/layout_title', 'block_title', 10, 4);
