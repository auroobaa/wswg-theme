<?php

add_action( 'wp_head', 'remove_edd_content_restriction_receipt_table_action' );
function remove_edd_content_restriction_receipt_table_action(){
//	global $my_class;
	remove_action( 'edd_payment_receipt_after_table', 'edd_cr_add_to_receipt', 1 );
}


/**
 * Add restricted content to confirmation page
 *
 * @since       1.3.0
 * @param       object $payment The payment we are processing
 * @param       array $edd_receipt_args The args for a given receipt
 * @return      void
 */
function wo_edd_cr_add_course_to_receipt( $payment, $edd_receipt_args ) {
	// Get the array of restricted pages for this payment
	$meta = edd_cr_get_restricted_pages( $payment->ID );

	// No pages? Quit!
	if ( empty( $meta ) ) {
		return;
	}
    
	?>

	<h3><?php echo apply_filters( 'edd_cr_payment_receipt_pages_title', __( 'Courses', 'wswg-theme' ) ); ?></h3>

	<table id="edd_purchase_receipt_pages" class="edd-table">
		<tbody>
			<?php foreach ( $meta as $download_id => $pages ) : ?>
				<tr>
					<td class="edd_purchase_receipt_pages">
						<ul>
							<?php foreach ( $pages as $page_id => $page_title ) : ?>
							    <?php $post_type = get_post_type($page_id);
							          if ($post_type == 'wswg-lessons') : 
                                        continue;
                                    else : ?>
								        <li><a href="<?php echo esc_url( get_permalink( $page_id ) ); ?>"><?php echo $page_title; ?></a></li>
								    <?php endif;
							endforeach; ?>
						</ul>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<?php
}
add_action( 'edd_payment_receipt_after_table', 'wo_edd_cr_add_course_to_receipt', 1, 2 );

remove_action( 'edd_complete_purchase', 'edd_trigger_purchase_receipt', 999, 1 );