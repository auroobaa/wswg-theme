<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * This is functionality added by Aurooba Ahmed
 *
 * @package WSWG_Theme
 */


/**
 * Remove paragraph tags around images
 */
function filter_ptags_on_images($content){
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');
add_filter('get_archive_description', 'filter_ptags_on_images');

/**
 * Remove paragraph tags in blockquotes
 */
function filter_ptags_on_blockquotes($content){
    $find = array('<blockquote class="ts-block"><p>', '</p></blockquote>');
    $replace = array('<blockquote class="ts-block">','</blockquote>');
    return str_replace($find, $replace, $content);
}
add_filter('the_content', 'filter_ptags_on_blockquotes');

/**
 * Return ACF sub field without wpautop
 */
function the_sub_field_without_wpautop( $field_name ) {
	
	remove_filter('acf_the_content', 'wpautop');
	
	echo the_sub_field( $field_name );
    
	add_filter('acf_the_content', 'wpautop');
}

/**
 * Deregister WordPress-included jQuery and include Google's
 */
if (!is_admin()) add_action("wp_enqueue_scripts", "wanderoak_jquery_enqueue", 11);
function wanderoak_jquery_enqueue() {
//    wp_deregister_script('jquery');
    
//    wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js", '', null, false);
    
    wp_enqueue_script('jquery');
//    wp_enqueue_script( 'wo-likes-js', get_template_directory_uri() . '/js/likes.js', array( 'jquery' ), '0.1', false );
//    wp_localize_script( 'wo-likes-js', 'simpleLikes', array(
//        'ajaxurl' => admin_url( 'admin-ajax.php' ),
//        'like' => __( 'Like', 'wswg' ),
//        'unlike' => __( 'Unlike', 'wswg' )
//) ); 
    
    wp_enqueue_script( 'wswg-mc-js', get_template_directory_uri() . '/js/wswg-mc.js', array( 'jquery' ), '0.1', true );
    wp_enqueue_script( 'wswg-magnific', get_template_directory_uri() . '/js/magnific-popup/jquery.magnific-popup.js', 'jquery', '010101', true );
    

    wp_enqueue_script( 'wswg-mc-post-js', get_template_directory_uri() . '/js/post-mc.js', array( 'jquery' ), '0.1', true );
    wp_localize_script( 'wswg-mc-post-js', 'wswgmcpost',
        array( 
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
        )
    );
  
}



/**
 * Returns a cleaner Navigation
 */

function cleanernav($location) {
    $clearnav = wp_nav_menu(array(
                    'theme_location' => $location,
                    'container' => false,
                    'items_wrap' => '%3$s', 
                    'echo' => false,
                    ));

    $find = array('><a', '<li');
    $replace = array('','<a');
    $newnav = str_replace($find, $replace, $clearnav);
    echo $newnav;
}

/**
 * ACF Theme Options Page
 */
//if( function_exists('acf_add_options_page') ) {
//	
//	acf_add_options_page();
//	acf_set_options_page_title( __('Theme Options') );
//}
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Options',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
        'parent_slug'	=> 'themes.php'
	));
	
}
/**
 * Numbered pagination
 */
function pagination_bar() {
    global $wp_query;
 
    $total_pages = $wp_query->max_num_pages;
 
    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));
 
        echo paginate_links(array(
            'base' => get_pagenum_link(1) . '%_%',
            'format' => '/page/%#%',
            'current' => $current_page,
            'total' => $total_pages,
        ));
    }
}

/**
 * Automatically add IDs to headings such as <h2></h2>
 */
function auto_id_headings( $content ) {
	$content = preg_replace_callback( '/(\<h[1-6](.*?))\>(.*)(<\/h[1-6]>)/i', function( $matches ) {
		if ( ! stripos( $matches[0], 'id=' ) ) :
			$matches[0] = $matches[1] . $matches[2] . ' id="' . sanitize_title( $matches[3] ) . '">' . $matches[3] . $matches[4];
		endif;
		return $matches[0];
	}, $content );
    return $content;
}
add_filter( 'the_content', 'auto_id_headings' );


/**
* Remove prefix on archive pages
*/

add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

});

/**
* Include editor style stylesheet.
*/
add_editor_style('editor-style.css');

/**
* Hide WordPress Update Nag to All But Admins
*/
function hide_update_notice_to_all_but_admin() {
    if ( !current_user_can( 'update_core' ) ) {
        remove_action( 'admin_notices', 'update_nag', 3 );
    }
}
add_action( 'admin_head', 'hide_update_notice_to_all_but_admin', 1 );

/**
* Disable the Emoji Nonsense
*/
function disable_wp_emojicons() {
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );
function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}

/**
* Media should also have categories, don't you agree?
*/

function add_categories_for_attachments() {
    register_taxonomy_for_object_type( 'category', 'attachment' );
}
add_action( 'init' , 'add_categories_for_attachments' );


/**
* No one needs the xmlrpc vulnerability.
*/
add_filter( 'xmlrpc_enabled', '__return_false' );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );

//include_once('wo-likes.php');

/**
 * Making sure Gravity Forms creates a customer in Stripe when a product transaction occurs.
 */
add_filter( 'gform_stripe_customer_id', function ( $customer_id, $feed, $entry, $form ) {
    if ( rgars( $feed, 'meta/transactionType' ) == 'product' && rgars( $feed, 'meta/feedName' ) == 'Create Customer and Payment' ) {
        $customer_meta = array();
 
        $metadata_field = rgars( $feed, 'meta/metaData' );
 
        foreach ($metadata_field as $metadata) {
            if ($metadata['custom_key'] == 'Email') {
                $email_field = $metadata['value'];
            }
        }
        
        if ( ! empty( $email_field ) ) {
            $customer_meta['email'] = gf_stripe()->get_field_value( $form, $entry, $email_field );
        }
 
        $customer = gf_stripe()->create_customer( $customer_meta, $feed, $entry, $form );
 
        return $customer->id;
    }
 
    return $customer_id;
}, 10, 4 );

/**
 * Remove Admin Bar For Subscribers
 */
add_action('after_setup_theme', 'cc_hide_admin_bar');
function cc_hide_admin_bar() {
    if (!current_user_can('edit_posts')) {
        add_filter('show_admin_bar', '__return_false');
    }
}

/**
 * Redirect back to Dashboard (as assigned in Theme Options) and don't allow access to WP admin for Subscribers.
 */
function wo_redirect_admin(){
    if ( ! defined('DOING_AJAX') && ! current_user_can('edit_posts') ) {
        $dashboard = get_field('dashboard', 'option');
        wp_redirect( $dashboard );
        exit;      
    }
}
add_action( 'admin_init', 'wo_redirect_admin' );

/**
 * Adds the date when the post was last modified.
 */
function lastmodified( $atts ) {  
    $lastmodified = get_the_modified_time('F jS, Y');
    $lastmodified_text = '<span class="modified-date">' . $lastmodified . '</span>';
    return $lastmodified_text; 
}
add_shortcode('lastmodified', 'lastmodified'); 

/**
 * Creating the Lessons Widget that will dynamically display all lessons of the course you're currently in.
 */
class WSWG_Author_Widget extends WP_Widget {
    function __construct() {
        parent::__construct(
            'wswg_author_widget', // Base ID
            'WSWG Author', // Name
            array('description' => __( 'Grabs the author of the post and displays it.'))
           );
    }
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        return $instance;
    }
    
    function widget($args, $instance) {
        extract( $args );
        echo $before_widget;
        if ( $title ) {
            echo $before_title . $title . $after_title;
        }
        $this->get_wswg_author();
        echo $after_widget;
    }
    
    function get_wswg_author() { //html
        global $post;
//        $course_id = wp_get_post_parent_id( $post->ID );
        wswg_article_author($post);
    }
} //end class Realty_Widget
register_widget('WSWG_Author_Widget');

function wswg_article_author($post) {
    $byline = sprintf(
		esc_html_x( 'Written by %s', 'post author', 'WSWG_Theme' ),
		'<span class="author"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);
    $avatar = get_avatar(get_the_author_meta( 'ID' ), 500);
    $bio = get_the_author_meta( 'description' );
    echo '<figure class="author-avatar">' . $avatar . '</figure>';
    echo '<div class="author-bio-wrapper"><div class="author-bio">' . $byline . ' — ' . $bio . '</div></div>';
}
