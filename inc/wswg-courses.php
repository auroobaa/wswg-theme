<?php
/**
 * This grabs and displays all the lessons of the current course in a list.
 * the $course_id variable is optional. If the $course_id parameter is not passed,
 * the function first checks to see if whether the current post is a course or lesson.
 * If it's a course, it grabs the ID and then uses it to grab all the associated lessons
 * in the order specified in the lessons custom field.
 * If it's a lesson, it saves the ID of the lesson in $current_lesson_id, and then grabs the course's id and stores that in $course_id.
 * If the $current_lesson_id exists and matches the ID of a lesson when the lessons are being put in a list, it adds the current-lesson class to it.
 */
function wswg_list_lessons($course_id = '') { 
    $arrow = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="15" height="20" viewBox="0 0 12 5">
<path d="M8.398 5.75q0 0.102-0.078 0.18l-3.641 3.641q-0.078 0.078-0.18 0.078t-0.18-0.078l-3.641-3.641q-0.078-0.078-0.078-0.18t0.078-0.18l0.391-0.391q0.078-0.078 0.18-0.078t0.18 0.078l3.070 3.070 3.070-3.070q0.078-0.078 0.18-0.078t0.18 0.078l0.391 0.391q0.078 0.078 0.078 0.18z"></path>
</svg>';
    
    if (!$course_id) {
        global $post;
        if (get_post_type($post->ID) == 'wswg-courses') { 
            $course_id = $post->ID; 
        } elseif (get_post_type($post->ID) == 'wswg-lessons') {
                $current_lesson_id = $post->ID;
                $course_id =  wp_get_post_parent_id( $post->ID );
        } else {
            return;
        }
        
    }
    if (get_field('course_structure', $course_id) == 'lessons') {
    //    var_dump($course_id);
        $lesson_order = get_field('lessons', $course_id);
        if (!$lesson_order) : echo '<p>This course doesn\'t have any lessons yet</p>'; return; endif;
        echo '<ul class="lessons">';
        foreach ($lesson_order as $lesson) :
    //    var_dump($current_lesson_id);
            if ($lesson == $current_lesson_id) :
                printf( '<li class="current-lesson"><a href="%s">%s</a></li>', get_permalink($lesson), get_the_title($lesson) );
            else :
                printf( '<li><a href="%s">%s</a></li>', get_permalink($lesson), get_the_title($lesson) );
            endif;
    //        echo $lesson->post_title;
        endforeach;
        echo '</ul>';
    } else {
        $modules = get_field('modules', $course_id);
        $module_list = array();
        echo '<ul class="modules">';
        foreach ($modules as $module) {
            $check_lesson_module = array_search($current_lesson_id, $module['lessons']);
            
            if ($check_lesson_module === false) {
                echo '<li class="module-name"><h4>' . $module['module'] . ' ' . $arrow . '</h4></li>';
            } else {
                echo '<li class="module-name show"><h4>' . $module['module'] . ' ' . $arrow . '</h4></li>';
            }
            echo '<ul class="module-lessons">';
            foreach ($module['lessons'] as $lesson) {
                if ($lesson == $current_lesson_id) :
                    printf( '<li class="current-lesson"><a href="%s">%s</a></li>', get_permalink($lesson), get_the_title($lesson) );
                else :
                    printf( '<li><a href="%s">%s</a></li>', get_permalink($lesson), get_the_title($lesson) );
                endif;
            }
            echo '</ul>';
        }
        echo '</ul>';
    }
 
}

function wswg_next_lesson($current_lesson_id = '') {
    if (!$current_lesson_id) {
        global $post;
        if (get_post_type($post->ID) == 'wswg-lessons') {
            $current_lesson_id = $post->ID;
            $course_id =  wp_get_post_parent_id( $post->ID );
        } else {
            return;
        }
    }
    if ( get_field('course_structure', $course_id) == 'modules') {
        $modules = get_field('modules', $course_id);
        $lesson_order = array();
        foreach ($modules as $module) {
            foreach ($module['lessons'] as $lesson) {
                $lesson_order[] = $lesson;
            }
        }
    } else {
        $lesson_order = get_field('lessons', $course_id);
    }
//    echo '<pre>' . var_export($lesson_order, true) . '</pre>';
    $current_lesson_position = array_search($current_lesson_id, $lesson_order);
//    echo '<pre>' . var_export($current_lesson_id, true) . '</pre>';
    $next_lesson = $lesson_order[$current_lesson_position+1];
    if ($lesson_order[$current_lesson_position] == end($lesson_order)) {
        //do nothing
    } else {
        printf( '<div class="next-lesson">Next Lesson: <a href="%s">%s</a></li> &rarr;</div>', get_permalink($next_lesson), get_the_title($next_lesson) );
    }

}

/**
 * Redirect all subscribers to the dashboard link specified in Theme Options
 */
function redirect_subscriber_dashboard( $redirect_to, $request, $user ) {
    //is there a user to check?
    if (isset($user->roles) && is_array($user->roles)) {
        //check for subscribers
        if (in_array('subscriber', $user->roles)) {
            // redirect them to another URL, in this case, the homepage 
            $redirect_to =  get_field('dashboard', 'option');
        }
    }

    return $redirect_to;
}
add_filter( 'login_redirect', 'redirect_subscriber_dashboard', 10, 3 );

function wswg_courses_classic($is_enabled, $post_type) {
	
	if ($post_type === 'wswg-courses') return false; // change book to your post type
	
	return $is_enabled;
	
}
add_filter('use_block_editor_for_post_type', 'wswg_courses_classic', 10, 2);

function wswg_ask_button() {
    $ask_form = get_field('ask_a_question_form', 'option');
    $before = '<div class="ask-button button-wrapper">';
    $button =  '<a href="#ask-form" class="tertiary-button open-popup-link">ask a question</a>';
    $form = '<div id="ask-form" class="mfp-hide overlay-popup">' . gravity_form( $ask_form['id'], true, false, false, null, true, '1', false ) . '</div>';
    $after = '<script>
                jQuery(document).ready(function() {
                    jQuery(".open-popup-link").magnificPopup({
                        type:"inline",
                        showCloseBtn: false,
                        midClick: true
                    });
                });
            </script>
        </div>';
    
    echo $before . $button . $form . $after;
    
}

// Extend Glossary plugin to include lesson text editor
function add_glossary_terms_to_acf_values( $value, $post_id, $field ) {
    if ( class_exists('Glossary_Handler') ) {
        $wporgglossary = new Glossary_Handler();
        $value = $wporgglossary->glossary_links($value);
    }

	// return
	return $value;
}

add_filter('acf/format_value/type=wysiwyg', 'add_glossary_terms_to_acf_values', 10, 3);

