<?php
/**
 * Creating the WSWG Lesson CPT
 */
function wswg_lessons_cpt() {
  $labels = array(
    'name'               => _x( 'We Start With Good Lessons', 'post type general name' ),
    'singular_name'      => _x( 'Lesson', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'guest-party' ),
    'add_new_item'       => __( 'Add New Lesson' ),
    'edit_item'          => __( 'Edit Lesson' ),
    'new_item'           => __( 'New Lesson' ),
    'all_items'          => __( 'All Lessons' ),
    'view_item'          => __( 'View Lesson' ),
    'search_items'       => __( 'Search Lessons' ),
    'not_found'          => __( 'No lessons found' ),
    'not_found_in_trash' => __( 'No lessons found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Lessons',
  );
    
  $args = array(
    'labels'        => $labels,
    'description'   => 'We Start With Good Lessons',
    'public'        => true,
    'supports'		=>	array( 'title', 'editor', 'author', 'revisions' ),
	 'show_in_menu' => 'edit.php?post_type=wswg-courses',
//     'menu_position' => 6,
    'menu_icon'     => 'dashicons-format-aside',  
    'has_archive'   => false,
    'hierarchical'  => false,
    'publicly_queryable' => true,
  );
  register_post_type( 'wswg-lessons', $args ); 
}
add_action( 'init', 'wswg_lessons_cpt' );

/**
 * Creating a non-hierarchial taxonomy for lesson vocabulary
 */
function wswg_lesson_vocab_tags() {
  $labels = array(
    'name'              => _x( 'Lesson Vocabulary', 'taxonomy general name' ),
    'singular_name'     => _x( 'Lesson Vocabulary', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Lesson Vocabulary' ),
    'all_items'         => __( 'All Lesson Vocabulary' ),
    'parent_item'       => __( 'Parent Lesson Vocabulary' ),
    'parent_item_colon' => __( 'Parent Lesson Vocabulary:' ),
    'edit_item'         => __( 'Edit Lesson Vocabulary' ), 
    'update_item'       => __( 'Update Lesson Vocabulary' ),
    'add_new_item'      => __( 'Add New Lesson Vocabulary' ),
    'new_item_name'     => __( 'New Lesson Vocabulary' ),
    'menu_name'         => __( 'Lesson Vocabulary' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
  );
  register_taxonomy( 'wswg_lesson_vocab', 'wswg-lessons', $args );
}
add_action( 'init', 'wswg_lesson_vocab_tags', 0 );

/**
 * Creating a hierarchial taxonomy for mini lessons
 */
function wswg_mini_lesson_types() {
  $labels = array(
    'name'              => _x( 'Mini Lesson Types', 'taxonomy general name' ),
    'singular_name'     => _x( 'Mini Lesson Type', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Mini Lesson Types' ),
    'all_items'         => __( 'All Mini Lesson Types' ),
    'parent_item'       => __( 'Parent Mini Lesson Type' ),
    'parent_item_colon' => __( 'Parent Mini Lesson Type:' ),
    'edit_item'         => __( 'Edit Mini Lesson Type' ), 
    'update_item'       => __( 'Update Mini Lesson Type' ),
    'add_new_item'      => __( 'Add New Mini Lesson Type' ),
    'new_item_name'     => __( 'New Mini Lesson Type' ),
    'menu_name'         => __( 'Mini Lesson Types' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'wswg_mini_lesson_types', 'wswg-lessons', $args );
}
add_action( 'init', 'wswg_mini_lesson_types', 0 );

add_action('admin_menu', 'wswg_lessons_admin_menu'); 
function wswg_lessons_admin_menu() { 
    add_submenu_page('edit.php?post_type=wswg-courses', 'Mini Lesson Types', 'Mini Lesson Types', 'manage_options', 'edit-tags.php?taxonomy=wswg_mini_lesson_types'); 
}  

/**
 * Making sure only child lessons show up in the Lessons Relationship Field on the Course Edit Screen
 */
add_filter('acf/fields/relationship/query/key=field_5acd8864678d7', 'show_child_lessons_on_course_screen_only', 10, 3);
function show_child_lessons_on_course_screen_only( $args, $field, $post_id ) {
	
    // only show children of the current post being edited
    $args['post_parent'] = $post_id;
	
	
	// return
    return $args;
    
}

/**
 * Adding the Course selector metabox to the Lesson edit screen so we can use it to make that course the parent page of this lesson
 */
function add_course_selector_metabox() {
	add_meta_box( 'lesson-parent', 'Course', 'course_selector_metabox', 'wswg-lessons', 'side', 'high' );
}
add_action( 'add_meta_boxes', 'add_course_selector_metabox' );

/**
 * Adding the Courses to the Course Selector Metabox
 */
function course_selector_metabox( $post ) {
	$post_type_object = get_post_type_object( $post->post_type );
	$pages = wp_dropdown_pages( array( 'post_type' => 'wswg-courses', 'selected' => $post->post_parent, 'name' => 'parent_id', 'show_option_none' => __( 'Select a Course' ), 'sort_column'=> 'post_title', 'echo' => 0 ) );
	if ( ! empty( $pages ) ) {
		echo $pages;
	}
}

/**
 * Adding new rewrite rules so that the link structure of lessons is {home}/{course}/{lesson}
 */
function lesson_rewrite_rules() {
	add_rewrite_tag('%wswg-lessons%', '([^/]+)', 'wswg-lessons=');
	add_permastruct('wswg-lessons', '/%wswg-course%/%wswg-lessons%', false);
	add_rewrite_rule('^courses/([^/]+)/([^/]+)/?','index.php?wswg-lessons=$matches[2]','top');
}
add_action( 'init', 'lesson_rewrite_rules' );

/**
 * Setting up the {home}/{course}/{lesson} permalink structure
 */
function lesson_permalink_structure($permalink, $post, $leavename) {
	$post_id = $post->ID;
	if($post->post_type != 'wswg-lessons' || empty($permalink) || in_array($post->post_status, array('draft', 'pending', 'auto-draft')))
	 	return $permalink;
	$parent = $post->post_parent;
	$parent_post = get_post( $parent );
	$permalink = str_replace('%wswg-course%', 'courses/' . $parent_post->post_name, $permalink);
	return $permalink;
}
add_filter('post_type_link', 'lesson_permalink_structure', 10, 3);

/**
 * Creating the Lessons Widget that will dynamically display all lessons of the course you're currently in.
 */
class WSWG_Lessons_Widget extends WP_Widget{
    function __construct() {
        parent::__construct(
            'wswg_lessons_widget', // Base ID
            'WSWG Lessons Widget', // Name
            array('description' => __( 'Displays the lessons in the current course and highlights the current lesson'))
           );
    }
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }

    function form($instance) {
        $title = '';
        if ( $instance ) {
            $title = esc_attr($instance['title']);
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'wswg-theme'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
	    </p>
    <?php }
    
    function widget($args, $instance) {
        extract( $args );
        $title = apply_filters('widget_title', $instance['title']);
        echo $before_widget;
        if ( $title ) {
            echo $before_title . $title . $after_title;
        }
        $this->get_wswg_lessons();
        echo $after_widget;
    }
    
    function get_wswg_lessons() { //html
        global $post;
//        $course_id = wp_get_post_parent_id( $post->ID );
        wswg_list_lessons();
    }
} //end class Realty_Widget
register_widget('WSWG_Lessons_Widget');

function wswg_lessons_classic($is_enabled, $post_type) {
	
	if ($post_type === 'wswg-lessons') return false; 
	
	return $is_enabled;
	
}
add_filter('use_block_editor_for_post_type', 'wswg_lessons_classic', 10, 2);

/**
 * Creating the Lessons Widget that will dynamically display all lessons of the course you're currently in.
 */
class WSWG_Ask_Widget extends WP_Widget{
    function __construct() {
        parent::__construct(
            'wswg_ask_widget', // Base ID
            'WSWG Ask Widget', // Name
            array('description' => __( 'Displays the Ask button and info on lessons/courses'))
           );
    }
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['description'] = strip_tags($new_instance['description']);
        return $instance;
    }

    function form($instance) {
        $title = '';
        if ( $instance ) {
            $title = esc_attr($instance['title']);
            $description = esc_attr($instance['description']);
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'wswg-theme'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
	    </p>
        <p>
            <label for="<?php echo $this->get_field_id('description'); ?>"><?php _e('Description', 'wswg-theme'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>" type="textarea" value="<?php echo $description; ?>" />
	    </p>
    <?php }
    
    function widget($args, $instance) {
        extract( $args );
        $title = apply_filters('widget_title', $instance['title']);
        $description = apply_filters('widget_title', $instance['description']);
        echo $before_widget;
        if ( $title ) {
            echo $before_title . $title . $after_title;
        }
        $this->get_ask_button();
        if ( $description ) {
            echo '<p class="description">' . $description . '</p>';
        }
        echo $after_widget;
    }
    
    function get_ask_button() { //html
        global $post;
//        $course_id = wp_get_post_parent_id( $post->ID );
        wswg_ask_button();
    }
} //end class Realty_Widget
register_widget('WSWG_Ask_Widget');

