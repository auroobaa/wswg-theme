/*
        updated JS file for use with ACF >= 5.7.0
*/


jQuery(document).ready(function($){
    if (typeof acf == 'undefined') { return; }

    /*
            In ACF >= 5.7.0 the acf.ajax object no longer exists so we can't extend it
            Instead we need to attach out event the old fashioned way
            and we need to do it using $(document).on because the element may
            be added dynamically and this is the only way to add events
    */

    $(document).on('change', '[data-key="field_5a5eb8c87a15e"] .acf-input select', function(e) {
        // we are not going to do anyting in this anonymous function
        // the reason is explained below
        // call function, we need to pass the event and jQuery object
        update_interests_on_list_change(e, $);
    });
    $(document).on('change', '[data-key="field_5a5eb8d17a15f"] .acf-input select', function(e) {
        // we are not going to do anyting in this anonymous function
        // the reason is explained below
        // call function, we need to pass the event and jQuery object
        update_groups_on_interest_change(e, $);
    });
//    $('[data-key="field_579376f522130"] .acf-input select').trigger('ready');
});

// the actual function is separate from the above change function
// the reason for this is that "this" has no real meaning in an anonymous 
// function as each call is a new JS object and we need "this" in order 
// to be to abort previous AJAX requests
function update_interests_on_list_change(e, $) {
    if (this.request) {
        // if a recent request has been made abort it
        this.request.abort();
    }

    // get the city select field, and remove all exisiting choices
    var interest_select = $('[data-key="field_5a5eb8d17a15f"] select');
    interest_select.empty();

    // get the target of the event and then get the value of that field
    var target = $(e.target);
    var list = target.val();

    if (!list) {
        // no state selected
        // don't need to do anything else
        return;
    }

    // set and prepare data for ajax
    var data = {
        action: 'load_mc_interest',
        list: list
    }

    // call the acf function that will fill in other values
    // like post_id and the acf nonce
    data = acf.prepareForAjax(data);

    // make ajax request
    // instead of going through the acf.ajax object to make requests like in <5.7
    // we need to do a lot of the work ourselves, but other than the method that's called
    // this has not changed much
    this.request = $.ajax({
        url: acf.get('ajaxurl'), // acf stored value
        data: data,
        type: 'post',
        dataType: 'json',
        success: function(json) {
            if (!json) {
                return;
            }
            // add the new options to the city field
            for(i=0; i<json.length; i++) {
                var item = '<option value="'+json[i]['value']+'">'+json[i]['label']+'</option>';
                interest_select.append(item);
            }
        }
    });

}

// the actual function is separate from the above change function
// the reason for this is that "this" has no real meaning in an anonymous 
// function as each call is a new JS object and we need "this" in order 
// to be to abort previous AJAX requests
function update_groups_on_interest_change(e, $) {
    if (this.request) {
        // if a recent request has been made abort it
        this.request.abort();
    }

    // get the city select field, and remove all exisiting choices
    var group_select = $('[data-key="field_5a5ec014c9b42"] select');
    group_select.empty();

    // get the target of the event and then get the value of that field
    var target = $(e.target);
    var interests = target.val();
    var list_field = $('[data-key="field_5a5eb8c87a15e"] select');
    var list = list_field.val();
    
    if (!list) {
        // no state selected
        // don't need to do anything else
        return;
    }

    // set and prepare data for ajax
    var data = {
        action: 'load_mc_groups',
        list: list,
        interests: interests
    }

    // call the acf function that will fill in other values
    // like post_id and the acf nonce
    data = acf.prepareForAjax(data);

    // make ajax request
    // instead of going through the acf.ajax object to make requests like in <5.7
    // we need to do a lot of the work ourselves, but other than the method that's called
    // this has not changed much
    this.request = $.ajax({
        url: acf.get('ajaxurl'), // acf stored value
        data: data,
        type: 'post',
        dataType: 'json',
        success: function(json) {
            if (!json) {
                return;
            }
            // add the new options to the city field
            for(i=0; i<json.length; i++) {
                var item = '<option value="'+json[i]['value']+'">'+json[i]['label']+'</option>';
                group_select.append(item);
            }
        }
    });

}