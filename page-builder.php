<?php
/**
 * Template Name: Page Builder
 *
 * @package Camp6R_Theme
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
            <header class="page-builder-header <?php if (get_field('centre_page_title')): echo 'centred more-margin'; endif; ?>">
                <?php
                    the_title( '<h1 class="entry-title">', '</h1>' );
                ?>
                <?php if ($subheading = get_field('subheading')): ?>
                <h5 class="builder-subheading"><?php echo $subheading; ?></h5>
                <?php endif; ?>
            </header><!-- .entry-header -->

            <?php
                // check if the flexible content field has rows of data
                if( have_rows('components') ):

                    // loop through the rows of data
                    while ( have_rows('components') ) : the_row();
            
                        if( get_row_layout() == 'editor' ):
            
                            get_template_part( 'builder/builder', 'text' );  
            
                        elseif( get_row_layout() == 'columns' ):
            
                            get_template_part( 'builder/builder', 'columns' );  
            
                        elseif( get_row_layout() == 'cta_builder' ):
            
                            get_template_part( 'builder/builder', 'cta' );  
            

                        endif;

                    endwhile;

                else :

                    // no layouts found

                endif; 


                ?>

            </section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer( 'bare' );
