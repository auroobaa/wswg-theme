<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WSWG_Theme
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
            <div class="content">
                <?php
                    while ( have_posts() ) : the_post();
                        the_content();
                    endwhile;
                ?>
            </div>
        <?php 
            $purchases = edd_get_users_purchased_products();
            if ($purchases) :
                foreach ($purchases as $purchase) :
                    $course = get_field('courses', $purchase->ID);
                    echo '<a class="course-card" href="' . get_permalink($course) . '">';
                    echo '<div class="featured-image">' . get_the_post_thumbnail($course) . '</div>';
                    echo '<div class="course-info"><h3>' . $purchase->post_title . '</h3>';
                    echo '<p>' . get_the_excerpt($course) . '</p></div>';
                    echo '</a>';
                endforeach;
            endif;
//            var_dump($purchases);
        ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer( 'bare' );
