<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package first-theme
 */

if ( ! is_active_sidebar( 'wswg-course-lessons' ) ) {
	return;
}
?>

<aside id="course-lessons-sidebar" class="widget-area" role="complementary">
	<?php dynamic_sidebar( 'wswg-course-lessons' ); ?>
</aside><!-- #secondary -->


<script>
jQuery( ".module-name" ).click(function() {
  jQuery(this).next().slideToggle();
});
</script>