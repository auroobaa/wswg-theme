<?php
/**
 * The template for displaying all single course pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WSWG_Theme
 */

get_header( ); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php

        if ( !edd_cr_user_has_access() ) {
            
            $restricted_to = get_post_meta( get_the_ID(), '_edd_cr_restricted_to', true );
            $edd = edd_cr_user_can_access('', $restricted_to);
            echo $edd['message'];

        } else {
            while ( have_posts() ) : the_post();

                get_template_part( 'template-parts/content', 'course' );

                //the_post_navigation();

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;

            endwhile; // End of the loop.
        }
		?>
        
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//if ( edd_cr_user_has_access() ) {
    get_sidebar('courses');
//}

get_footer( 'bare' );
