<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WSWG_Theme
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
        <div class="entry-header-content">
<!--
            <div class="likes">
                <?php echo get_simple_likes_button( get_the_ID() ); ?>
            </div>
-->

            <?php
                if ( is_single() ) {
                    the_title( '<h1 class="entry-title">', '</h1>' );
                } else {
                    the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
                } ?>
        </div>

        <?php if ( 'post' === get_post_type() ) : ?>
        <div class="entry-meta">
            <?php wswg_posted_on(); ?>
        </div><!-- .entry-meta -->
        <?php
        endif; ?>
        <?php if (get_field('disclaimernote')) : ?>
	    <div class="disclaimernote">
	        <?php the_field('disclaimernote'); ?>
	    </div>
	    <?php endif; ?>

	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'wswg' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wswg' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
