<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WSWG_Theme
 */

?>
<article>
<h1><?php the_title(); ?></h1>
<?php
    // check if the flexible content field has rows of data
    if( have_rows('lesson_builder') ):

        // loop through the rows of data
        while ( have_rows('lesson_builder') ) : the_row();

            if( get_row_layout() == 'mini_lesson' ):

                get_template_part( 'builder/builder', 'mini-lesson' );

            elseif( get_row_layout() == 'text' ):

                get_template_part( 'builder/builder', 'text' );  
    
            elseif( get_row_layout() == 'exercise' ):

                get_template_part( 'builder/builder', 'exercise' );  


            endif;

        endwhile;

    else :

        // no layouts found

    endif; 
    wswg_next_lesson();
?>
</article>

<?php 
$grocery_list = array( 'ground beef', 'spaghetti', 'pasta sauce', 'italian seasoning', 'chili flakes', 'wraps', 'cheddar', 'apples' );

function meal_prep() {
    go_shopping($globals['grocery_list']);
    $meal_prep_menu = array ( 'Bolognese pasta', 'quesadillas', 'sliced apples' );
    
    // a lot of pretend functionality that helps us with our coder meal prep
}

?>

