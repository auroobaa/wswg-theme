<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WSWG_Theme
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'index-articles'); ?>>
	<header class="index-post-header">
		<?php
            the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
            $categories = get_the_category();
            if ( ! empty( $categories ) ) {
                echo '<a class="post-category" href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '"> / ' . esc_html( $categories[0]->name ) . '</a>';
            }
            
        ?>
	</header><!-- .entry-header -->
</article><!-- #post-## -->
